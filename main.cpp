#include <iostream>
#include <memory>

#include "Observer/BasicSmokeDetector.h"
#include "Observer/BasicThermometer.h"
#include "Observer/FireDetector.h"
#include "Iterator/SensorAggregator.h"
#include "Visitor/SetFireAlarmVisitor.h"
#include "Visitor/GetAverageTemperatureVisitor.h"

using namespace std;

int main() {

    auto smokeDetector = make_shared<BasicSmokeDetector>();
    auto smokeDetector2 = make_shared<BasicSmokeDetector>();
    auto thermometer = make_shared<BasicThermometer>();
    auto thermometer2 = make_shared<BasicThermometer>();
    auto thermometer3 = make_shared<BasicThermometer>();

    shared_ptr<FireDetector> secondFD = make_shared<FireDetector>();

    auto aVis = make_shared<SetFireAlarmVisitor>();
    aVis->setSmokeObserver(secondFD);
    aVis->setTemperatureObserver(secondFD);

    auto avTempVis = make_shared<GetAverageTemperatureVisitor>();

    SensorAggregator livingRoomSensors;
    livingRoomSensors.AddSensor(thermometer);
    livingRoomSensors.AddSensor(thermometer2);
    livingRoomSensors.AddSensor(thermometer3);
    livingRoomSensors.AddSensor(smokeDetector);
    livingRoomSensors.AddSensor(smokeDetector2);

    for(auto it = livingRoomSensors.getIterator();!it->isDone();it->next()){
        it->get()->acceptVisitor(aVis);
        it->get()->checkState();
    }
    cout<<"##########"<<endl;
    smokeDetector->detectSmoke();
    thermometer->setTemp(80.3);
    thermometer3->setTemp(12.3);
    cout<<"##########"<<endl;
    for(auto it = livingRoomSensors.getIterator();!it->isDone();it->next()){
        it->get()->checkState();
        it->get()->acceptVisitor(avTempVis);
    }
    cout<<"##########"<<endl;
    cout<<"average temperature is: "<<avTempVis->getAverageVisitedTemperature()<<"*C"<<endl;

}
