//
// Created by Tomasz Rzepka on 26/04/15.
//

#include "SensorIterator.h"

std::shared_ptr<ISensor> SensorIterator::first() {
    m_current = 0;
    return (m_current<m_size?m_container->at(m_current):nullptr);
}

std::shared_ptr<ISensor> SensorIterator::next() {
    m_current++;
    return (m_current<m_size?m_container->at(m_current):nullptr);
}

bool SensorIterator::isDone() {
    return m_current>=m_size;
}

std::shared_ptr<ISensor> SensorIterator::get() {
    return (m_current<m_size?m_container->at(m_current):nullptr);
}

SensorIterator::SensorIterator(std::shared_ptr<std::vector<std::shared_ptr<ISensor>>> ptr, unsigned long position):
        m_container(ptr), m_current(position) {}

