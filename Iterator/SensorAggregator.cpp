//
// Created by Tomasz Rzepka on 26/04/15.
//

#include "SensorAggregator.h"
#include "SensorIterator.h"

using namespace std;

shared_ptr<IIterator> SensorAggregator::getIterator() {
    return make_shared<SensorIterator>(m_container,0);
}

void SensorAggregator::AddSensor(shared_ptr<ISensor> ptr) {
    m_container->push_back(ptr);
}
