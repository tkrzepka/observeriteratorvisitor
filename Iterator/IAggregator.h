//
// Created by Tomasz Rzepka on 26/04/15.
//

#pragma once
#include <memory>
#include "IIterator.h"

class IAggregator{
public:
    virtual std::shared_ptr<IIterator> getIterator() = 0;
};