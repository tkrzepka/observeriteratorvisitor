//
// Created by Tomasz Rzepka on 26/04/15.
//

#pragma once

#include <memory>
#include "../Observer/ISensor.h"

class IIterator{
public:
    virtual std::shared_ptr<ISensor> first() = 0;
    virtual std::shared_ptr<ISensor> next() = 0;
    virtual std::shared_ptr<ISensor> get() = 0;
    virtual bool isDone() = 0;
};