//
// Created by Tomasz Rzepka on 26/04/15.
//

#pragma once

#include "IIterator.h"
#include "../Observer/ISensor.h"

#include <memory>
#include <vector>

class SensorIterator : public IIterator {

public:
    virtual std::shared_ptr<ISensor> first() override;
    virtual std::shared_ptr<ISensor> next() override;
    virtual bool isDone() override;
    SensorIterator(std::shared_ptr<std::vector<std::shared_ptr<ISensor>>>, unsigned long);
    virtual std::shared_ptr<ISensor> get() override;

private:
    std::shared_ptr<std::vector<std::shared_ptr<ISensor>>> m_container;
    unsigned long m_current{0};
    unsigned long m_size{m_container->size()};
};
