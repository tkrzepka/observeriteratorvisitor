//
// Created by Tomasz Rzepka on 26/04/15.
//

#pragma once

#include <memory>
#include <vector>
#include "IAggregator.h"

class SensorAggregator : public IAggregator {
public:
    virtual std::shared_ptr<IIterator> getIterator() override;
    void AddSensor(std::shared_ptr<ISensor>);
private:
    std::shared_ptr<std::vector<std::shared_ptr<ISensor>>> m_container{std::make_shared<std::vector<std::shared_ptr<ISensor>>>()};
};
