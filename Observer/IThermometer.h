//
// Created by Tomasz Rzepka on 26/04/15.
//

#pragma once
#include <memory>
#include <iostream>
#include "ITemperatureObserver.h"
#include "ISensor.h"
#include "../Visitor/IVisitor.h"

class IThermometer : public ISensor{
public:
    virtual void addTemperatureObserver(std::shared_ptr<ITemperatureObserver>) = 0;
    virtual float getTemperature() const = 0;

    virtual void acceptVisitor(std::shared_ptr<IVisitor> ptr) override;

private:
    virtual void checkState() override;
};

