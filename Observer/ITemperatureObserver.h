//
// Created by Tomasz Rzepka on 26/04/15.
//

#pragma once

class ITemperatureObserver{
public:
    virtual void update(float) = 0;
};