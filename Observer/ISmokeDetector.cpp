//
// Created by Tomasz Rzepka on 26/04/15.
//

#include <iostream>
#include "ISmokeDetector.h"

void ISmokeDetector::checkState() {
    std::cout<<(hasSmokeBeenDetected()?"Smoke has been detected!":"Smoke has not been detected.")<< std::endl;
}

void ISmokeDetector::acceptVisitor(std::shared_ptr<IVisitor> ptr) {
    ptr->operation(this);
}
