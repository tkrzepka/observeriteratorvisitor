//
// Created by Tomasz Rzepka on 26/04/15.
//

#pragma once

#include "ITemperatureObserver.h"
#include "ISmokeObserver.h"

class FireDetector:public ITemperatureObserver, public ISmokeObserver {
public:
    virtual void update();
    virtual void update(float d);
    void alarm();
    FireDetector();

private:
    static int m_detectorsCreated;
    const int m_id;
    const int m_tempAlarm{60};
};
