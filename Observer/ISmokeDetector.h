//
// Created by Tomasz Rzepka on 26/04/15.
//

#pragma once

#include <memory>
#include <iostream>
#include "ISmokeObserver.h"
#include "ISensor.h"

class ISmokeDetector: public ISensor{
public:
    virtual void addSmokeObserver(std::shared_ptr<ISmokeObserver>) = 0;
    virtual bool hasSmokeBeenDetected() const = 0;

    virtual void acceptVisitor(std::shared_ptr<IVisitor> ptr) override;

private:
    virtual void checkState() override;
};

