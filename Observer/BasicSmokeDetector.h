//
// Created by Tomasz Rzepka on 26/04/15.
//

#pragma once

#include "ISmokeDetector.h"

#include <list>
#include <memory>

class BasicSmokeDetector: public ISmokeDetector {
public:
    virtual void addSmokeObserver(std::shared_ptr<ISmokeObserver> p_observer) override;
    virtual bool hasSmokeBeenDetected() const override;
    void detectSmoke();

private:
    bool m_smokeDetected{false};
    std::list<std::shared_ptr<ISmokeObserver>> m_observers;
};
