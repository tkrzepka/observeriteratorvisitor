//
// Created by Tomasz Rzepka on 26/04/15.
//

#include "FireDetector.h"
#include <iostream>

using namespace std;

int FireDetector::m_detectorsCreated{0};

void FireDetector::update() {
    cout<<"smoke has been detected"<<endl;
    alarm();
}

void FireDetector::update(float d) {
    if(d>=m_tempAlarm) {
        cout << "high temperature detected " << d << "*C" << endl;
        alarm();
    }
}

FireDetector::FireDetector() :m_id(m_detectorsCreated++){
}

void FireDetector::alarm() {
    cout<<"FIRE DETECTED(detector id="<<m_id<<")"<<endl;
}
