//
// Created by Tomasz Rzepka on 26/04/15.
//

#include "BasicSmokeDetector.h"

void BasicSmokeDetector::addSmokeObserver(std::shared_ptr<ISmokeObserver> p_observer) {
    m_observers.push_back(p_observer);
}

bool BasicSmokeDetector::hasSmokeBeenDetected() const {
    return m_smokeDetected;
}

void BasicSmokeDetector::detectSmoke() {
    m_smokeDetected = true;
    for(auto& observer : m_observers)
        observer->update();
}
