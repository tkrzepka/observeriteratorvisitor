//
// Created by Tomasz Rzepka on 26/04/15.
//

#pragma once

#include "IThermometer.h"
#include "ITemperatureObserver.h"

#include <list>
#include <memory>

class BasicThermometer: public IThermometer{
public:
    virtual void addTemperatureObserver(std::shared_ptr<ITemperatureObserver> p_observer) override;
    virtual float getTemperature() const override;
    void setTemp(float p_temp);
private:
    float m_temp{0.0};
    std::list<std::shared_ptr<ITemperatureObserver>> m_observers;
};