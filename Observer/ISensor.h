//
// Created by Tomasz Rzepka on 26/04/15.
//

#pragma once
#include <memory>
#include "../Visitor/IVisitor.h"

class ISensor{
public:
    virtual void checkState() = 0;
    virtual void acceptVisitor(std::shared_ptr<IVisitor>) = 0;
};