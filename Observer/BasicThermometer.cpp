//
// Created by Tomasz Rzepka on 26/04/15.
//

#include "BasicThermometer.h"

void BasicThermometer::addTemperatureObserver(std::shared_ptr<ITemperatureObserver> p_observer) {
    m_observers.push_back(p_observer);
}

float BasicThermometer::getTemperature() const {
    return m_temp;
}

void BasicThermometer::setTemp(float p_temp) {
    m_temp = p_temp;
    for(auto& observer : m_observers)
        observer->update(m_temp);
}
