//
// Created by Tomasz Rzepka on 26/04/15.
//

#include "IThermometer.h"

void IThermometer::checkState() {
    std::cout<<"current temperature is: "<< getTemperature()<< std::endl;
}

void IThermometer::acceptVisitor(std::shared_ptr<IVisitor> ptr) {
    ptr->operation(this);
}
