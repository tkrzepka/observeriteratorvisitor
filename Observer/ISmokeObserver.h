//
// Created by Tomasz Rzepka on 26/04/15.
//

#pragma once

class ISmokeObserver{
public:
    virtual void update() = 0;
};