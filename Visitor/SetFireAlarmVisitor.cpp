//
// Created by Tomasz Rzepka on 27/04/15.
//

#include "SetFireAlarmVisitor.h"
#include "../Observer/IThermometer.h"
#include "../Observer/ISmokeDetector.h"

using namespace std;

void SetFireAlarmVisitor::operation(ISmokeDetector *detector) {
    if(m_smokeObserver == nullptr)
        throw runtime_error("smoke observer not set in SetFireAlarmVisitor");
    detector->addSmokeObserver(m_smokeObserver);
}

void SetFireAlarmVisitor::operation(IThermometer *thermometer) {
    if(m_temperatureObserver == nullptr)
        throw runtime_error("temperature observer not set in SetFireAlarmVisitor");
    thermometer->addTemperatureObserver(m_temperatureObserver);
}

void SetFireAlarmVisitor::setSmokeObserver(shared_ptr<ISmokeObserver> ptr) {
    m_smokeObserver = ptr;
}

void SetFireAlarmVisitor::setTemperatureObserver(shared_ptr<ITemperatureObserver> ptr) {
    m_temperatureObserver = ptr;
}
