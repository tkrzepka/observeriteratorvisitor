//
// Created by Tomasz Rzepka on 27/04/15.
//

#include "GetAverageTemperatureVisitor.h"
#include "../Observer/IThermometer.h"
#include "../Observer/ISmokeDetector.h"

using namespace std;

void GetAverageTemperatureVisitor::operation(ISmokeDetector *detector) {
//do nothing
}

void GetAverageTemperatureVisitor::operation(IThermometer *thermometer) {
    m_temperature *= m_thermometersCount++;
    m_temperature += thermometer->getTemperature();
    m_temperature /= m_thermometersCount;
}

float GetAverageTemperatureVisitor::getAverageVisitedTemperature() {
    return m_temperature;
}
