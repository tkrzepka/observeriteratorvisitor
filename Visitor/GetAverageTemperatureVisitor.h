//
// Created by Tomasz Rzepka on 27/04/15.
//

#pragma once

#include "IVisitor.h"

class GetAverageTemperatureVisitor : public IVisitor{
public:
    float getAverageVisitedTemperature();
    virtual void operation(ISmokeDetector *detector) override;
    virtual void operation(IThermometer *thermometer) override;

private:
    unsigned long m_thermometersCount{0};
    float m_temperature{0.0};
};
