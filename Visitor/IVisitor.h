//
// Created by Tomasz Rzepka on 27/04/15.
//

#pragma once
#include <memory>
//#include "../Observer/IThermometer.h" Why do you not compile?
//#include "../Observer/ISmokeDetector.h" Why do you not compile?

class IThermometer;
class ISmokeDetector;

class IVisitor{
public:
    virtual void operation(ISmokeDetector*) = 0;
    virtual void operation(IThermometer*) = 0;
};