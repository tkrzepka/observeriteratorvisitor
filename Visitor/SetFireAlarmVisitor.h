//
// Created by Tomasz Rzepka on 27/04/15.
//

#pragma once

#include "IVisitor.h"
#include "../Observer/FireDetector.h"

class SetFireAlarmVisitor : public IVisitor{

public:
    void setSmokeObserver(std::shared_ptr<ISmokeObserver>);
    void setTemperatureObserver(std::shared_ptr<ITemperatureObserver>);
    virtual void operation(ISmokeDetector *detector) override;
    virtual void operation(IThermometer *thermometer) override;

private:
    std::shared_ptr<ISmokeObserver> m_smokeObserver{nullptr};
    std::shared_ptr<ITemperatureObserver> m_temperatureObserver{nullptr};
};
